Changes made to this version:
-Added sticky header
-added hamburger menu for mobile
-added section id and linked in main nav
-added line break after headings
-added hover effect for articles 
-added RTL stack on mobile in the About section
-added hover effect on email subscribe button 
-added hover effect on social media icons
-I sectioned out the different rows with comments, just to make it easier to navigate.
-I added a placeholder Google Analytics shell
-added drop shadow to navigation bar


Pain Points:
-I struggled getting the column in the about section to stack right over left. Once I got it figured out, it messed with the breakpoint. 
I have not found a solution to this yet.
-I tried to make the header transparent with an effect to switch to a colored background at a certain viewport, but it 
kept throwing off the positioning of the background slider.
-I used the same CSS and HTML for the footer navigation links, but they stack vertically instead of horizontal for some reason. I tried to modify the CSS 
by using a footer id and changing the style.

Random thought:
I was able to get the About section to look the same way it did on the .png (where the image was 100% width within its container).
But I just thought it looked better for the image to be aligned with the content above it. I've included a screenshot I took of
when it was aligned. 